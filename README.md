# Vacuum CapRover Deployment

Clone this project for a CapRover Deployment of Docker-Vacuum cleanup service

<p>
<img src="https://gitlab.com/pascal.cantaluppi/vacuum/-/raw/master/img/banner.png" alt="CapRover" />
</p>

&rarr; https://marcopeg.com/2019/docker-vacuum

## Getting started

### Setup

```bash
npm i caprover -g
caprover login
```

### Deploy

```bash
git clone https://gitlab.com/pascal.cantaluppi/vacuum.git
cd vacuum
caprover deploy
```

## Captain definition file

```json
{
  "schemaVersion": 2,
  "imageName": "marcopeg/docker-vacuum:latest"
}
```

## Captain Configuration

### HTTP Settings

<p>
<img src="https://gitlab.com/pascal.cantaluppi/vacuum/-/raw/master/img/http.png" alt="HTTP Settings" />
</p>

### Environmental Variables

<p>
<img src="https://gitlab.com/pascal.cantaluppi/vacuum/-/raw/master/img/env.png" alt="Environment variables" />
</p>


```
VACUUM_RULES=[{"match":"(.*)","retain":1}]
VACUUM_DELAY=1000
```

### Persistent Directories

<p>
<img src="https://gitlab.com/pascal.cantaluppi/vacuum/-/raw/master/img/dir.png" alt="Directories" />
</p>
